from http import client

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch, RequestsHttpConnection
from elasticsearch.client import IndicesClient
from baseapp.constants import DEFAULT_PAGINATION_SIZE

INITIALIZATION_ERR = 'Cannot initiate Elastic Server'
DATA_NO_ACCESS = 'Cannot access data from ES Server'


class ESClient():
    es_index = es_type = search = client = index_name = None
    pagination_params = {'size': DEFAULT_PAGINATION_SIZE}
    filters = {}

    def __init__(self, host, port, index_name):
        config = [{'host': host, 'port': port}]
        client = Elasticsearch(config)
        self.index_name = index_name
        if not client.ping():
            raise Exception(INITIALIZATION_ERR)

        if client and not self.search:
            self.client = client

    def filter(self, type_name):
        """
        Execute query with filters
        """
        search = Search(using=self.client, index=self.index_name, doc_type=type_name)
        range_filters = self.filters.get('range_filters', None)
        term_filters = self.filters.get('term_filters', None)
        exclude_filters = self.filters.get('exclude_filters', None)
        query_filters = self.filters.get('query_filters', None)

        if range_filters:
            for range_filter in range_filters:
                search = search.query('range', **range_filter)

        if term_filters:
            for term_filter in term_filters:
                search = search.filter('term', **term_filter)

        if exclude_filters:
            for exclude_filter in exclude_filters:
                search = search.exclude('term', **exclude_filter)

        if query_filters:
            search = search.query(query_filters)

        try:
            response = search.extra(**self.pagination_params).execute()

            return response
        except Exception as e:
            # TODO log exception
            raise Exception(DATA_NO_ACCESS)

    def set_service_search_filters(self, filters):
        """
        Set filters
        """
        if filters:
            self.filters = filters

    def set_pagination(self, per_page, page_number):
        """
        Pagination
        """
        if page_number and per_page:
            self.pagination_params = {'from': (page_number - 1), 'size': per_page}

    def create_or_update(self, type_name, is_new, id, payload):
        """
        Create or update in ES
        """
        data = dict(index=self.index_name,
                    doc_type=type_name,
                    id=id,
                    refresh=True)

        del payload['_id']
        if is_new is not None:
            data['body'] = {'doc': payload}
            self.client.update(**data)
        else:
            data['body'] = payload
            self.client.create(**data)

    def delete(self, type_name, delete_query, size=''):
        """
        Delete records from elastic search.
        """
        search = Search(using=self.client, index=self.index_name, doc_type=type_name)
        search = search.query(delete_query)
        if size:
            search = search.extra(size=size)
        try:
            response = search.delete()
        except Exception as e:
            # TODO log exception
            raise Exception(DATA_NO_ACCESS)

    def update_elasticsearch_by_query(self, update_query, type_name):
        """ Performs mass update on elasticsearch records using query.
        Args:
            update_query: Query to search and update elasticsearch records
            type_name: Type name of the elasticsearch document

        Returns:
            bool - True if success
        """
        self.client.update_by_query(body=update_query, doc_type=type_name, index=self.index_name)

        return True

    def put_mapping(self, modelName):
        """
        Create index if not available and then create mapping.
        """
        try:
            indices_client = IndicesClient(client=self.client)
            index = modelName._meta.es_index_name
            type = modelName._meta.es_type_name
            if not indices_client.exists(index):
                indices_client.create(index=index)
            indices_client.put_mapping(
                doc_type=type,
                body=modelName._meta.es_mapping,
                index=index
            )
        except Exception as e:
            raise Exception('Failed to recreate service index')
