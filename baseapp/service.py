
class BaseServiceClass:
    """
    convert proto object to python dict
    """
    def proto_to_dict(self, request):
        form_dict = {}
        for key in request.data.DESCRIPTOR.fields_by_name:
            form_dict[key] = getattr(request.data, key)
        return form_dict


class DotDict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__
