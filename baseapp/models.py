from django.db import models
from es.es_client import ESClient
import uuid
from baseapp.utils import K, Konstants
from . import message
import django.db.models.options as options
from django.contrib.postgres.fields import JSONField

options.DEFAULT_NAMES += ('es_index_name', 'es_type_name', 'es_mapping', 'es_host', 'es_port')


STOP_EDIT_DELETE_CHOICES = Konstants(
    K(can_edit_delete=0, label='Can Edit/Delete'),
    K(cannot_edit_delete=1, label='Can not Edit/Delete'),
    K(can_edit_not_delete=2, label='Can Edit but not Delete')
)

USER_TYPES = Konstants(
    K(admin=1, label='Admin'),
    K(franchisee=2, label='Franchisee'),
    K(counter=3, label='Counter'),
    K(customer=4, label='Customer')
)

BOOKING_AGENTS = Konstants(
    K(online='online', label='Online'),
    K(counter='counter', label='Counter'),
    K(franchisee='franchisee', label='Franchisee')
)

CATEGORY_TYPES = Konstants(
    K(other=1),
    K(general=2),
    K(tatkal=3))

GENDER_CHOICES = Konstants(
    K(Male='M'),
    K(Female='F'),
    K(Transgender='T')
)

PAYMENT_STATUS = Konstants(
    K(INI=1, label='Initialized'),
    K(GPA=2, label='Gateway Payment Aborted'),
    K(GPC=3, label='Gateway Payment Confirmed and cannot create PNR'),
    K(CNF=4, label='Gateway payment Confirmed and created PNR')
)

METRIC_TYPES = (
    ('fixed_amount', 'Fixed Amount'),
    ('percentage', 'Percentage'))

BOOKING_AGENT_CHOICES = (
    ('counter', 'Counter'),
    ('online', 'Online'),
    ('franchisee', 'Franchisee'))

STATE_CHOICES = Konstants(
    K(AP=1, label='Andhra Pradesh'),
    K(AR=2, label='Arunachal Pradesh'),
    K(AS=3, label='Assam'),
    K(BH=4, label='Bihar'),
    K(BH=5, label='Chhattisgarh'),
    K(GA=6, label='Goa'),
    K(GJ=7, label='Gujarat'),
    K(HR=8, label='Haryana'),
    K(HP=9, label='Himachal Pradesh'),
    K(JK=10, label='Jammu & Kashmir'),
    K(JH=11, label='Jharkhand'),
    K(KN=12, label='Karnataka'),
    K(KL=13, label='Kerala'),
    K(MP=14, label='Madhya Pradesh'),
    K(MH=15, label='Maharashtra'),
    K(MN=16, label='Manipur'),
    K(MG=17, label='Meghalaya'),
    K(MZ=18, label='Mizoram'),
    K(NL=19, label='Nagaland'),
    K(OR=20, label='Orissa'),
    K(PB=21, label='Punjab'),
    K(RJ=22, label='Rajasthan'),
    K(SK=23, label='Sikkim'),
    K(TN=24, label='Tamil Nadu'),
    K(TG=25, label='Telangana'),
    K(TR=26, label='Tripura'),
    K(UK=27, label='Uttarakhand'),
    K(UP=28, label='Uttar Pradesh'),
    K(WB=29, label='West Bengal'),
)

NOTIFICATION_STATUS = Konstants(
    K(active='active', label='Active'),
    K(inactive='inactive', label='Inactive'),
    K(upcoming='upcoming', label='Upcoming'),
    K(expired='expired', label='Expired')
)

AUTHENTICATION_MEDIA = Konstants(
    K(facebook='facebook', label='Facebook'),
    K(google='google', label='Google'),
    K(twitter='twitter', label='Twitter'),
    K(instagram='instagram', label='Instagram'),
    K(linkedin='linkedin', label='LinkedIn'),
)

TICKET_STATUS = Konstants(
    K(CNF=1, label='Confirmed'),
    K(CAN=2, label='Cancelled'),
    K(CNT=3, label='Cancelled Ticket')
)

BOOKING_CATEGORIES = Konstants(
    K(online='online', label='Online'),
    K(counter='counter', label='Counter'),
    K(all='all', label='All')
)

FARE_CATEGORY = Konstants(
    K(lt100=1, label='<=100'),
    K(gt100=2, label='>100')
)

PAYMENT_TYPES = Konstants(
    K(hdfc=1, label='HDFC Bank'),
    K(billdesk=2, label='BillDesk')
)

DEPENDENCY_NOTIFICATION = Konstants(
    K(no_notification=0, label='No notification'),
    K(service_reapply=1, label='Service Reapply'),
    K(fare_regenerate=2, label='Fare Regenerate')
)

EXCEPTION_APP = Konstants(
    K(bff_base_pack='BP', label='BFF Base Pack'),
    K(service_base_pack='SP', label='Service Base Pack'),
    K(portal_bff='PB', label='Portal BFF'),
    K(admin_bff='AB', label='Admin BFF'),
    K(booking_service='BS', label='Booking Service'),
    K(operation_service='OS', label='Operation Service'),
    K(fare_service='FS', label='Fare Service'),
    K(reporting_service='RS', label='Reporting Service'),
    K(payment_service='PS', label='Payment Service'),
    K(user_service='US', label='User Service'),
    K(bill_desk='BD', label='BillDesk')
)


EXC_NO = Konstants(
    K(one=1),
    K(two=2),
    K(three=3),
    K(four=4),
    K(five=5),
    K(six=6),
    K(seven=7),
    K(eight=8),
    K(nine=9),
    K(ten=10),
    K(eleven=11),
    K(twelve=12),
    K(thirteen=13),
    K(fourteen=14),
    K(fifteen=15),
    K(sixteen=16),
    K(seventeen=17),
    K(eighteen=18),
    K(nineteen=19),
    K(twenty=20),
    K(twentyone=21),
    K(twentytwo=22),
    K(twentythree=23),
    K(twentyfour=24),
    K(twentyfive=25),
    K(twentysix=26),
    K(twentyseven=27),
    K(twentyeight=28),
    K(twentynine=29),
    K(thirty=30),
    K(thirtyone=31),
    K(thirtytwo=32)
)


class BaseModelClass(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=100, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.CharField(max_length=100, blank=True)
    modification_status = models.IntegerField(choices=STOP_EDIT_DELETE_CHOICES.choices(), default=None, blank=True,
                                              null=True)

    def delete(self, *args, **kwargs):
        """
        delete object after checking dependency
        """
        dependency_check = kwargs.pop('dependency_check', True)
        force_delete = kwargs.pop('force_delete', False)
        if (self.modification_status == STOP_EDIT_DELETE_CHOICES.cannot_edit_delete or
                self.modification_status == STOP_EDIT_DELETE_CHOICES.can_edit_not_delete) and not force_delete:
            raise Exception(message.CANNOT_DELETE)
        if dependency_check:
            for rel in self._meta.get_fields():
                try:
                    # To check if there is a relationship with at least one related object
                    related = rel.related_model.objects.filter(**{rel.field.name: self})
                    if related.exists() and dependency_check:
                        return False
                except AttributeError:
                    pass
        return super(BaseModelClass, self).delete(*args, **kwargs)

    def has_dependency_data(self, related_to):
        """
        Check model has dependency data
        """
        try:
            return bool(getattr(self, related_to).first())
        except AttributeError as e:
            # TODO: log exception
            pass

    class Meta:
        abstract = True


class ESModel(models.Model):
    """
    Model for elastic search
    """
    def es_repr(self):
        """
        ES Mapping for fields
        """
        data = {}
        mapping = self._meta.es_mapping
        data['_id'] = self.pk
        for field_name in mapping['properties'].keys():
            data[field_name] = self.field_es_repr(field_name)

        return data

    def set_pk(self):
        """
        Set primary key as it is throwing error via model
        """
        self.pk = uuid.uuid4()

    def field_es_repr(self, field_name):
        """
        get es field value
        """
        config = self._meta.es_mapping['properties'][field_name]
        if hasattr(self, 'get_es_%s' % field_name):
            field_es_value = getattr(self, 'get_es_%s' % field_name)()
        else:
            if config['type'] == 'object':
                related_object = getattr(self, field_name)
                field_es_value = {}
                field_es_value['_id'] = related_object.pk
                for prop in config['properties'].keys():
                    field_es_value[prop] = getattr(related_object, prop)
            else:
                field_es_value = getattr(self, field_name)

        return field_es_value

    def save(self, *args, **kwargs):
        """
        Override model save - create/update ES also
        """
        is_new = self.pk

        # Set Primary key before save
        if self.pk is None:
            self.pk = uuid.uuid4()

        # check whether values need to be saved in model also
        if self._meta.managed:
            super(ESModel, self).save(*args, **kwargs)

        payload = self.es_repr()
        es_client = ESClient(self._meta.es_host, self._meta.es_port, self._meta.es_index_name)
        es_client.create_or_update(self._meta.es_type_name, is_new, self.pk, payload)

    class Meta:
        abstract = True


class BookingOtp(BaseModelClass):
    otp = models.CharField(max_length=10)
    expiry_time = models.DateTimeField()
    no_of_attempts = models.IntegerField()
    resend_attempts = models.IntegerField()

    class Meta:
        abstract = True


class FailedQueue(BaseModelClass):
    task_id = models.UUIDField()
    task_name = models.CharField(max_length=100)
    error_message = models.TextField()
    failed_time = models.DateTimeField(auto_now_add=True)
    data = JSONField()
    identifier = models.CharField(max_length=200)

    class Meta:
        abstract = True
        unique_together = ('task_id', 'identifier')
