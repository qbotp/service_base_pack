from django import forms
from . import message
from .models import STOP_EDIT_DELETE_CHOICES


class BaseModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # Forms inheriting the base class must pass user_id as argument.
        self.user_id = kwargs.pop('user_id', None)

        # To differentiate between edit and create operation.

        super(BaseModelForm, self).__init__(*args, **kwargs)

        self.is_edit = False
        if kwargs['instance']:
            self.is_edit = True
            if 'created_by' in self.fields:
                del self.fields['created_by']

    def save(self, *args, **kwargs):
        kwargs['commit'] = False
        obj = super(BaseModelForm, self).save(*args, **kwargs)
        if obj.modification_status == STOP_EDIT_DELETE_CHOICES.cannot_edit_delete:
            raise Exception(message.CANNOT_EDIT)
        # Sets updated_by or created_by fields depending on the operation performed.
        if self.is_edit:
            obj.updated_by = self.user_id
        else:
            obj.created_by = self.user_id
        obj.save()
        return obj

    class Meta:
        fields = ['created_by', 'updated_by']
