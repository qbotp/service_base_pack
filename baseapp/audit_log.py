import grpc
import json
import ast
from django.conf import settings
from loggrpcservice import logservices_pb2, logservices_pb2_grpc
from baseapp.utils import K, Konstants

MICRO_SERVICE = Konstants(
    K(user='user', label='User'),
    K(operation='operation', label='Operation'),
    K(log='log', label='Log'),
    K(fare='fare', label='Fare'),
    K(booking='booking', label='Booking'),
    K(report='report', label='Report'),
    K(payment='payment', label='Payment')
)

LOG_TYPE = Konstants(
    K(audit='audit', label='Audit'),
    K(transaction='transaction', label='Transaction'),
    K(exception='exception', label='Exception'),
    K(error='error', label='Error'),
    K(notice='notice', label='Notice')
)

APP = Konstants(
    K(user='user', label='User'),
    K(role='role', label='Role'),
    K(state='state', label='State'),
    K(district='district', label='District'),
    K(pickup_point='pickup_point', label='Pickup Point'),
    K(place='place', label='Place'),
    K(organisation='organisation', label='Organization'),
    K(corporation='corporation', label='Corporation'),
    K(reports='reports', label='Reports'),
    K(division='division', label='Division'),
    K(depot='depot', label='Depot'),
    K(bus_station='bus_station', label='Bus Station'),
    K(counter='counter', label='Counter'),
    K(franchisee='franchisee', label='Franchisee'),
    K(layout='layout', label='Layout'),
    K(Class='Class', label='Class'),
    K(route='route', label='Route'),
    K(service='service', label='Service'),
    K(payment='payment', label='Payment'),
    K(refund='refund', label='Refund'),
    K(ticket='ticket', label='Ticket'),
    K(portal_booking='portal_booking', label='Portal Booking'),
    K(search='search', label='Search Services'),
    K(seat_allocation='seat_allocation', label='Seat Allocation'),
    K(fare='fare', label='Fare'),
    K(zone='zone', label='Zone'),
    K(ticket_series='ticket_series', label='Ticket Series'),
    K(conductor_chart='conductor_chart', label='Conductor Chart'),
    K(cancel_service='cancel_service', label='Cancel Service'),
    K(location='location', label='Location'),
    K(general_settings='general_settings', label='General Settings'),
    K(booking_params='booking_params', label='Booking Parameters'),
    K(passenger_category='passenger_category', label='Passenger Category'),
    K(fare_add_on='fare_add_on', label='Fare Add On'),
    K(fare_charge='fare_charge', label='Fare Charge'),
    K(group_discount='group_discount', label='Group Discount'),
    K(notification='notification', label='Notification'),
    K(seat_quota='seat_quota', label='Seat Quota'),
    K(service_class='service_class', label='Service Class'),
    K(slab='slab', label='Slab'),
    K(levy='levy', label='Levy'),
    K(rate='rate', label='Rate'),
    K(round_off='round_off', label='Round Off'),
    K(booking_charge='booking_charge', label='Booking Charge'),
    K(class_pass='class_pass', label='Class Pass'),
    K(day_end_report='day_end_report', label='Day End Report'),
    K(transaction_history='transaction_history', label='Transaction History'),
    K(corporation_pass='corporation_pass', label='Pass'),
    K(complaint='complaint', label='Complaint'),
    K(top_routes='top_routes', label='Top Routes'),
    K(enable_operator='enable_operator', label='Enable Operator'),
)

LOG_ACTION = Konstants(
    K(add='add', label='Add'),
    K(edit='edit', label='Edit'),
    K(delete='delete', label='Delete'),
    K(fetch='fetch', label='Fetch'),
    K(login='login', label='Login'),
    K(reset='reset', label='Reset'),
    K(cancellation='cancellation', label='Cancellation'),
    K(copy='copy', label='Copy'),
    K(generate='generate', label='Generate'),
    K(modify='modify', label='Modify'),
    K(enable='enable', label='Enable'),
    K(disable='disable', label='Disable'),
    K(change_password='change_password', label='Change Password'),
    K(set_password='set_password', label='Set Password'),
    K(check='check', label='Check'),
    K(apply='apply', label='Apply'),
    K(revoke='revoke', label='Revoke'),
)


def push_to_audit_log(log_info):
    """ Write exception to auditlog service.
    Args:
        log_info: Information for log
    Returns:

    """
    try:
        auditlog_channel = grpc.insecure_channel(settings.CHANNELS['auditlog_channel'])
        stub = logservices_pb2_grpc.LogmanagementStub(auditlog_channel)
        new_value = build_json(log_info.get('new_value', None))
        old_value = build_json(log_info.get('old_value', None))
        response = stub.create_log(
            logservices_pb2.log_creation_request(service=log_info['service'],
                                                 log_type=log_info['log_type'],
                                                 module=log_info['app'],
                                                 action=log_info['action'],
                                                 message=log_info.get('message', None),
                                                 new_value=new_value,
                                                 old_value=old_value,
                                                 user_id=log_info.get('user_id', None),
                                                 traceback=log_info.get('traceback', None)))

    except Exception:
        pass


def push_to_log_service(log_info):
    """ Write fare to auditlog service.
    Args:
        log_info: Information for log
    Returns:

    """
    try:
        auditlog_channel = grpc.insecure_channel(settings.CHANNELS['auditlog_channel'])
        stub = logservices_pb2_grpc.LogmanagementStub(auditlog_channel)
        new_value = build_json(log_info.get('new_value', None))
        old_value = build_json(log_info.get('old_value', None))
        response = stub.push_to_log(
            logservices_pb2.log_creation_request(service=log_info['service'],
                                                 log_type=log_info['log_type'],
                                                 module=log_info['app'],
                                                 action=log_info['action'],
                                                 message=log_info.get('message', None),
                                                 new_value=new_value,
                                                 old_value=old_value,
                                                 user_id=log_info.get('user_id', None)))

    except Exception:
        pass


def build_json(new_value):
    try:
        json_value = json.dumps(ast.literal_eval(new_value))
    except Exception:
        json_value = json.dumps({'value': new_value})
    return json_value
