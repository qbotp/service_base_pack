from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q


class ListingMixin:

    def build_search_query(self, query_set, search_columns, search_value):
        """
        Search query - set search fields as list
        """
        if search_value:
            query = Q()
            for field in search_columns:
                values = {field + '__icontains': str(search_value)}
                query |= Q(**values)

            query_set = query_set.filter(query)

        return query_set

    def build_filter_query(self, query_set, filters):
        """
        Apply filters
        """
        for filter in filters:
            query_set = query_set.filter(**filter)

        return query_set

    def paginate(self, query_set, per_page, page_no):
        """
        Pagination
        """
        if page_no or per_page:
            paginator = Paginator(query_set, per_page)
            try:
                query_set = paginator.page(page_no)
            except PageNotAnInteger as e:
                # TODO: log exception
                query_set = paginator.page(1)
            except EmptyPage as e:
                # TODO: log exception
                query_set = paginator.page(paginator.num_pages)

        return query_set

    def apply_default_sort(self, query_set):
        """
        Apply default sort by created date
        """
        return query_set.order_by('-created_at')

    def filter_by_date(self, date, query_set):
        return query_set.filter(Q(schedule__effective_from__lte=date,
                                  schedule__effective_to__gte=date) |
                                Q(schedule__effective_from__lte=date,
                                  schedule__effective_to__isnull=True))

    def search_in_array(self, query_set, search_column, search_value):
        """
        Search for a value inside an array field
        """
        search_term = [str(search_value)]
        if search_value:
            values = {search_column + '__contains': search_term}
            query = Q(**values)

            query_set = query_set.filter(query)

        return query_set
