import logging
import requests
import random
import smtplib
from io import StringIO, BytesIO
import xhtml2pdf.pisa as pisa
import os
from django.template import Context
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from django.conf import settings
import pytz
from datetime import datetime, timedelta, date

from baseapp.constants import MOBILE_NUMBER_PREFIX, ALPHABET
from baseapp.message import COMMON_EXCEPTION_MSG, CANNOT_EDIT, CANNOT_DELETE


def get_form_errors_if_any(form):
    """
    Process form errors to required format
    """
    form_data = [(k, v[0]) for k, v in form.errors.items()]
    validations = [{'field': key, 'error_message': value} for key, value in form_data]

    return validations


def send_email_notification(subject, template, **kwargs):
    """
    send email notification
    Args:
        subject: email subject
        template: email template
        **kwargs: keyword args
    Returns:
        bool - True if send successfully otherwise False
    """
    try:
        msg = MIMEMultipart()
        msg['From'] = settings.EMAIL_HOST_USER
        if isinstance(kwargs['to'], str):
            msg['To'] = kwargs['to']
        msg['Subject'] = subject
        msg.attach(MIMEText(template, 'html'))
        if kwargs.get('file'):
            msg = set_attachment(msg, kwargs.get('file'))

        text = msg.as_string()
        server = smtplib.SMTP(settings.EMAIL_HOST_ADDRESS)
        server.starttls()
        server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        server.sendmail(settings.EMAIL_HOST_USER, kwargs['to'], text)
        server.quit()

        return True
    except Exception as e:
        logging.exception(e)
        return False


def set_attachment(msg, file):
    """
    Set attachment in email
    Args:
        msg: email msg
        file: file which is to be attached
    Returns:
        msg object
    """
    part = MIMEBase('application', 'octet-stream')
    part.set_payload(open(file, "rb").read())
    file_path, file_name = os.path.split(file)
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename=%s" % file_name)
    msg.attach(part)

    return msg


def send_sms(mobile_number, **kwargs):
    try:
        mobile_number = MOBILE_NUMBER_PREFIX + mobile_number
        if settings.SEND_SMS and kwargs.get('mobile_otp'):
            if settings.TEST_SMS:
                mobile_number = settings.TEST_MOBILE_NUMBER
            response = requests.post(settings.SMS_BASE_URL %
                                     (settings.SMS_USERNAME, settings.SMS_PASSWORD, settings.SMS_SENDER,
                                      mobile_number, str(kwargs.get('mobile_otp'))))
            return response.ok
        return False
    except Exception as e:
        logging.exception(e)


def generate_otp(pin_length):
    otp = random.sample(range(10 ** (pin_length - 1), 10 ** pin_length), 1)[0]
    return otp


class K:
    def __init__(self, label=None, **kwargs):
        assert(len(kwargs) == 1)
        for k, v in kwargs.items():
            self.id = k
            self.v = v
        self.label = label or self.id


class Konstants:
    def __init__(self, *args):
        self.klist = args
        for k in self.klist:
            setattr(self, k.id, k.v)

    def choices(self):
        return [(k.v, k.label) for k in self.klist]

    def display(self, k):
        for ks in self.klist:
            if k == ks.v:
                return ks.label
        return ""

    def getkey(self, value):
        for k in self.klist:
            if k.v == value:
                return k.id

        return ""

    def __getitem__(self, k):
        return self.display(k)


def get_date_time_in_local_timezone(data, format="%Y-%m-%d %H:%M", from_zone='UTC', to_zone='Asia/Kolkata'):
    """
    Method to get date in local timezone
    Args:
        data (string): date to be converted
        from_zone (string): The from time zone
        to_zone (string): Local timezone
    Returns:
        date_in_local : date in local timezone
    """
    tz1 = pytz.timezone(from_zone)
    tz2 = pytz.timezone(to_zone)
    data = datetime.strptime(data, format)
    data = tz1.localize(data)
    date_in_local = data.astimezone(tz2)
    date_in_local = date_in_local.strftime(format)

    return date_in_local


def find_dict_by_key_value(list_dict, key, value):
    """
    Method to return the dict from a list by finding key and value
    Args:
        list_dict (list): List Dict
        key (str): Key
        value (str): value
    """
    return next((item for item in list_dict if item[key] == value), None)


def nearest_data_through_effective_from(object_list, effective_date):
    """
    Method to get nearest quota based on passenger  category
    Args:
        service (object): Service table object
        passenger category (int): Integer
        pick up start date (string): stringified pick up start date
    """
    nearest_data = None
    object_date_list = [datetime.strptime(date['effective_from'], '%Y-%m-%d').date() for date in object_list]
    effective_date = datetime.strptime(effective_date, '%Y-%m-%d').date()
    for value in object_date_list:
        if effective_date < value:
            object_date_list.remove(value)
    if object_date_list:
        closest_date = min(object_date_list, key=lambda date: nearest_date(date, effective_date))
        nearest_data = find_dict_by_key_value(object_date_list, 'effective_from', closest_date.strftime('%Y-%m-%d'))
    return nearest_data


def nearest_date(date, base_date):
    """
    Method to get nearest date to a pickup_start_date from a list of date
    Args:
        date (date): date object
        pickup_start_date (date): pick up start date obhect
    """
    delta = date - base_date if date > base_date else timedelta.min
    return delta


def generate_pdf(template, template_values, corporation):
    cntxt = Context(template_values)
    html = template.render(cntxt)
    result = BytesIO()
    pdf_name = corporation + '_' + date.today().strftime('%d-%m-%Y') + '.pdf'
    stored_pdf = open(settings.PDF_DIRECTORY + pdf_name, "w+b")
    pdf = pisa.pisaDocument(StringIO(html), dest=stored_pdf)
    password = generate_password()
    return pdf_name, password


def generate_password():
    alphabet = ALPHABET
    pw_length = settings.PDF_PASSWORD_LENGTH
    password = ""

    for i in range(pw_length):
        next_index = random.randrange(len(alphabet))
        password += alphabet[next_index]

    return password


def get_exception_msg(app, exc, number):
    exc = str(exc)
    if exc == CANNOT_EDIT or exc == CANNOT_DELETE or settings.DEBUG:
        return exc
    else:
        return app + bin(number)[2:].zfill(4) + ":" + COMMON_EXCEPTION_MSG
